package buzanov.iteco;

import buzanov.iteco.enumerated.BodyType;
import buzanov.iteco.enumerated.Color;
import buzanov.iteco.enumerated.EngineFuelType;
import buzanov.iteco.parts.Body;
import buzanov.iteco.parts.Cabin;
import buzanov.iteco.parts.Engine;

public class LadaFactory implements CarFactory{
    @Override
    public Body createBody() {
        return new Body("Granta", Color.EGGPLANT, BodyType.SEDAN);
    }

    @Override
    public Engine createEngine() {
        return new Engine(106, EngineFuelType.PETROL);
    }

    @Override
    public Cabin createCabin() {
        return new Cabin( Color.WHITE, "Кожзам");
    }
}
