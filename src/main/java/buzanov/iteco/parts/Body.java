package buzanov.iteco.parts;

import buzanov.iteco.enumerated.BodyType;
import buzanov.iteco.enumerated.Color;

public class Body {
    private String name;

    private Color color;

    private BodyType bodyType;

    public Body(String name, Color color, BodyType bodyType) {
        this.name = name;
        this.color = color;
        this.bodyType = bodyType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public BodyType getBodyType() {
        return bodyType;
    }

    public void setBodyType(BodyType bodyType) {
        this.bodyType = bodyType;
    }
}


