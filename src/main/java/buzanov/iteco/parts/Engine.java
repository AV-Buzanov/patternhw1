package buzanov.iteco.parts;

import buzanov.iteco.enumerated.EngineFuelType;

public class Engine {
    private int power;

    private EngineFuelType fuelType;

    public Engine(int power, EngineFuelType fuelType) {
        this.power = power;
        this.fuelType = fuelType;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public EngineFuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(EngineFuelType fuelType) {
        this.fuelType = fuelType;
    }
}
