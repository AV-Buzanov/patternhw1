package buzanov.iteco.parts;

import buzanov.iteco.enumerated.Color;

public class Cabin {
    private Color color;

    private String material;

    public Cabin (Color color, String material) {
        this.color = color;
        this.material = material;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }
}
