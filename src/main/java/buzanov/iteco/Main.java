package buzanov.iteco;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите марку автомобиля:");
        String model = scanner.next();
        CarFactory carFactory = CarFactoryHelper.create(model);
        CarPrinter.print(carFactory.createBody(), carFactory.createEngine(), carFactory.createCabin(), model);
        scanner.close();
    }
}
