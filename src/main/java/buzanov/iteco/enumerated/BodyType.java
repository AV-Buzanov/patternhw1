package buzanov.iteco.enumerated;

public enum BodyType {
    SEDAN("Седан"),
    HATCHBACK("Хэтчбек");

    private final String value;

    public String getValue() {
        return value;
    }

    BodyType(String value) {
        this.value = value;
    }
}
