package buzanov.iteco.enumerated;

public enum EngineFuelType {
    PETROL("Бензин"),
    DIESEL("Дизель"),
    GAS("Газ");

    private final String value;

    public String getValue() {
        return value;
    }

    EngineFuelType(String value) {
        this.value = value;
    }
}
