package buzanov.iteco.enumerated;

public enum Color {
    BLACK("Черный"),
    WHITE("Белый"),
    RED("Красный"),
    EGGPLANT("Баклажан");

    private final String value;

    public String getValue() {
        return value;
    }

    Color(String value) {
        this.value = value;
    }
}
