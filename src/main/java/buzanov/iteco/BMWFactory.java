package buzanov.iteco;

import buzanov.iteco.enumerated.BodyType;
import buzanov.iteco.enumerated.Color;
import buzanov.iteco.enumerated.EngineFuelType;
import buzanov.iteco.parts.Body;
import buzanov.iteco.parts.Cabin;
import buzanov.iteco.parts.Engine;

public class BMWFactory implements CarFactory{
    @Override
    public Body createBody() {
        return new Body("X5", Color.BLACK, BodyType.HATCHBACK);
    }

    @Override
    public Engine createEngine() {
        return new Engine(320, EngineFuelType.DIESEL);
    }

    @Override
    public Cabin createCabin() {
        return new Cabin( Color.BLACK, "Кожа");
    }
}
