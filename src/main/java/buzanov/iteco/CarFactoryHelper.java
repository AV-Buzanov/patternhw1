package buzanov.iteco;

public class CarFactoryHelper {

    public static CarFactory create(String model){
        switch (model){
            case "BMW":
                return new BMWFactory();
            case "Lada":
                return new LadaFactory();
            case "Opel":
                return new OpelFactory();
            default:
                throw new IllegalStateException("Unexpected value: " + model);
        }
    }
}
