package buzanov.iteco;

import buzanov.iteco.parts.Body;
import buzanov.iteco.parts.Cabin;
import buzanov.iteco.parts.Engine;

interface CarFactory {

    Body createBody();

    Engine createEngine();

    Cabin createCabin();
}
