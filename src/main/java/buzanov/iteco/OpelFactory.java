package buzanov.iteco;

import buzanov.iteco.enumerated.BodyType;
import buzanov.iteco.enumerated.Color;
import buzanov.iteco.enumerated.EngineFuelType;
import buzanov.iteco.parts.Body;
import buzanov.iteco.parts.Cabin;
import buzanov.iteco.parts.Engine;

public class OpelFactory implements CarFactory{
    @Override
    public Body createBody() {
        return new Body("Astra", Color.RED, BodyType.HATCHBACK);
    }

    @Override
    public Engine createEngine() {
        return new Engine(115, EngineFuelType.PETROL);
    }

    @Override
    public Cabin createCabin() {
        return new Cabin( Color.WHITE, "Кожзам");
    }
}
