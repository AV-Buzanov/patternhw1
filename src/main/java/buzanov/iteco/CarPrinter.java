package buzanov.iteco;

import buzanov.iteco.parts.Body;
import buzanov.iteco.parts.Cabin;
import buzanov.iteco.parts.Engine;

public class CarPrinter {
    public static void print(Body body, Engine engine, Cabin cabin, String model) {
        System.out.printf("***Характеристики автомобиля марки %s ***", model);
        System.out.println("");
        System.out.println("");
        System.out.println("***Кузов***");
        System.out.printf("***Тип: %s", body.getBodyType().getValue());
        System.out.println("");
        System.out.printf("***Цвет: %s", body.getColor().getValue());
        System.out.println("");
        System.out.println("***************");
        System.out.println("***Двигатель***");
        System.out.printf("***Мощность: %s л.с", engine.getPower());
        System.out.println("");
        System.out.printf("***Тип топлива: %s", engine.getFuelType().getValue());
        System.out.println("");
        System.out.println("***************");
        System.out.println("***Салон***");
        System.out.println("");
        System.out.printf("***Цвет: %s", cabin.getColor().getValue());
        System.out.println("");
        System.out.printf("***Материал: %s", cabin.getMaterial());

    }
}
